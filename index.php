<?php
$title = 'Home';
$bg1 = '#eee2d2';
$bg2 = $fg1;
$bg3 = '#EED';
$bg4 = '';
$fg1 = '#532';
$fg2 = '#6a4a3d';
$fg3 = '#a54';
$fg4 = '#45a';
function pickColor($a,$b){
echo (isset($a)&&trim($a)!='') ? $a : $b;
}
$ios = (strstr($_SERVER['HTTP_USER_AGENT'],'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'],'iPod') || strstr($_SERVER['HTTP_USER_AGENT'],'iPad'));
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if lt IE 7 ]><html dir="ltr" lang="en-US" class="ie ie6 lte7 lte8 lte9"><![endif]-->
<!--[if IE 7 ]><html dir="ltr" lang="en-US" class="ie ie7 lte7 lte8 lte9"><![endif]-->
<!--[if IE 8 ]><html dir="ltr" lang="en-US" class="ie ie8 lte8 lte9"><![endif]-->
<!--[if IE 9 ]><html dir="ltr" lang="en-US" class="ie ie9 lte9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="ltr" lang="en-US"><!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="Description" />
<meta name="keywords" content="Keywords" />
<meta name="author" content="Benjamin J DeLong" />
<meta name="robots" content="noindex, nofollow, noarchive" />
<meta name="viewport" content="width=960">
<link rel="shortcut icon" href="/favicon.ico" />
<link rel="img_src" href="something.jpg" />
<title>

<?php
if(isset($title)) echo $title . ' - ';
?>
Bear News
</title>
<style>
/*
Font Face
*/

@font-face {
  font-family: 'h1';
  font-style: normal;
  font-weight: normal;
  src: local('Fredericka the Great'), local('FrederickatheGreat'), url('http://themes.googleusercontent.com/static/fonts/frederickathegreat/v2/7Es8Lxoku-e5eOZWpxw18vc3vBAn7YvtW9hNjxWZBR0.woff') format('woff');
}
@font-face {
  font-family: 'h2';
  font-style: normal;
  font-weight: 700;
  src: local('Open Sans Condensed Bold'), local('OpenSans-CondensedBold'), url('http://themes.googleusercontent.com/static/fonts/opensanscondensed/v6/gk5FxslNkTTHtojXrkp-xONSK5BxN3NFS4EJkViHIqo.woff') format('woff');
}
@font-face {
  font-family: 'fancy';
  font-style: normal;
  font-weight: 400;
  src: local('Codystar'), url('http://themes.googleusercontent.com/static/fonts/codystar/v1/BraFEMRumMsVbWgqTQIWRPesZW2xOQ-xsNqO47m55DA.woff') format('woff');
}
@font-face {
  font-family: 'p';
  font-style: normal;
  font-weight: 400;
  src: local('Telex-Regular'), url('http://themes.googleusercontent.com/static/fonts/telex/v1/QBUn5F7b-9Ulki7K75J1Bw.woff') format('woff');
}

/* My Extra Classes and Styles */
h1, h2, h3, h4, h5 { margin:0;padding: .83em 0; }
.png { background:inherit; }
a {text-decoration:none;}
a img {border:none}
.button { background: #FEFCEA;
background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZlZmNlYSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNmMWRhMzYiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
background: -moz-linear-gradient(top, #FEFCEA 0%, #F1DA36 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#FEFCEA), color-stop(100%,#F1DA36));
background: -webkit-linear-gradient(top, #FEFCEA 0%,#F1DA36 100%);
background: -o-linear-gradient(top, #FEFCEA 0%,#F1DA36 100%);
background: -ms-linear-gradient(top, #FEFCEA 0%,#F1DA36 100%);
background: linear-gradient(top, #FEFCEA 0%,#F1DA36 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fefcea', endColorstr='#f1da36',GradientType=0 );
color: black;
padding: 2px 10px;
border-radius: 3px;
margin: 0 10px;
cursor: pointer;
border: 1px solid #F1DA36;
}
.button:hover { 
background: #F1DA36;
border-color: #A98;
}

/* End of My Extra Classes and Styles */

/* Begin Standardized CSS */
body { font-size:12px; }
.clearfix:after {
content: ".";
display: block;
height: 0;
clear: both;
visibility: hidden;
}
article,aside,details,figcaption,figure,footer,header,hgroup,nav,section {
display: block;
}
audio,canvas,video {
display: inline-block;
*display: inline;
*zoom: 1;
}
audio:not([controls]) {
display: none;
}
[hidden] {
display: none;
}
html {
overflow:auto;
font-size: 100%; /* 1 */
-webkit-text-size-adjust: 100%; /* 3 */
-ms-text-size-adjust: 100%; /* 3 */
}
body {
margin: 0;
overflow:hidden;
}
body,button,input,select,textarea {
font-family: sans-serif;
}
a:focus {
outline: thin dotted;
}
a:hover,a:active {
outline: 0;
}
h1 {
font-size: 2em;
}
abbr[title] {
border-bottom: 1px dotted;
}
b, strong {
font-weight: bold;
}
blockquote {
margin: 1em 40px;
}
dfn {
font-style: italic;
}
mark {
background: #ff0;
color: #000;
}
pre,code,kbd,samp {
font-family: monospace, serif;
_font-family: 'courier new', monospace;
font-size: 1em;
}
pre {
white-space: pre;
white-space: pre-wrap;
word-wrap: break-word;
}
q {
quotes: none;
}
q:before,q:after {
content: '';
content: none;
}
small {
font-size: 75%;
}
sub,sup {
font-size: 75%;
line-height: 0;
position: relative;
vertical-align: baseline;
}
sup {
top: -0.5em;
}
sub {
bottom: -0.25em;
}
ul,ol {
margin: 0;
padding: 0;
margin-left:60px;
}
dd {
margin: 0 0 0 40px;
}
nav ul,nav ol, li {
list-style: square;
list-style-image: none;
}
img {
border: 0; /* 1 */
-ms-interpolation-mode: bicubic; /* 2 */
}
svg:not(:root) {
overflow: hidden;
}
figure {
margin: 0;
}
form {
margin: 0;
}
fieldset {
border: 1px solid #c0c0c0;
margin: 0 2px;
padding: 0.35em 0.625em 0.75em;
}
legend {
border: 0; /* 1 */
*margin-left: -7px; /* 2 */
}
button,input,select,textarea {
font-size: 100%; /* 1 */
margin: 0; /* 2 */
vertical-align: baseline; /* 3 */
*vertical-align: middle; /* 3 */
}
button,input {
line-height: normal; /* 1 */
}
button,input[type="button"], input[type="reset"], input[type="submit"] {
cursor: pointer; /* 1 */
-webkit-appearance: button; /* 2 */
*overflow: visible; /* 3 */
}
input[type="checkbox"],input[type="radio"] {
box-sizing: border-box; /* 1 */
padding: 0; /* 2 */
}
input[type="search"] {
-webkit-appearance: textfield; /* 1 */
-moz-box-sizing: content-box;
-webkit-box-sizing: content-box; /* 2 */
box-sizing: content-box;
}
input[type="search"]::-webkit-search-decoration {
-webkit-appearance: none;
}
button::-moz-focus-inner,input::-moz-focus-inner {
border: 0;
padding: 0;
}
textarea {
overflow: auto; /* 1 */
vertical-align: top; /* 2 */
}
table {
border-collapse: collapse;
border-spacing: 0;
}
/* body {-moz-user-select: none;-khtml-user-select: none;-ie-user-select: none;} --disable selecting text--*/ 

@media only screen and (max-device-width: 480px){
/*--- iPhone only CSS here ---*/
}

@media handheld {
/*--- cellphone only CSS here --*/
}

@media print {
/*--- print only CSS here --*/
}

/* Standard CSS ends here */

html { background:<?php pickColor($bg2,'#ccc'); ?>; }
body { color:<?php pickColor($fg1,'#444'); ?>; }
body > div {padding:2px 50px;position:relative;background:<?php pickColor($bg2,'#F0F0F0'); ?>;}
.container { width:800px; margin:0 auto; }
.subcontainer { width:550px; margin:0 auto; }
#floatingTop { top:0;z-index:1;}
.fixIt { position:fixed;padding:0; }
#floatHome { display:none;width: 150px;height: 20px;background: url(images/BNB2.png) no-repeat;text-indent: -9999px;position:absolute;right:0;top:16px; }
#floatingTop.fixIt #floatHome { display:block; }
#one { padding-top: 75px; padding-bottom:25px;font-family:h1;font-size:28px; }
#header a {width: 150px;height: 20px;background: url(images/BNB2.png) no-repeat;text-indent: -9999px;display: block;float:right;margin-top:8px;}
#two { background:<?php pickColor($bg3,'#F3F4F5'); ?>;border-top:solid <?php pickColor($fg3,'#ccc'); ?> 1px;border-bottom:solid <?php pickColor($fg2,'#e8e8e8'); ?> 1px;padding-top:7px;padding-bottom:2px; }
#nav { width:970px;overflow:hidden;height:38px;padding-left:20px; }
#nav a { font-size:26px;text-shadow:#F0F0F0 1px 1px 0px;font-family:h2;color:<?php pickColor($fg1,'#444'); ?>; font-weight:bold;padding:0 24px; }
#nav a span {  border-bottom:2px solid <?php pickColor($bg2,'#F0F0F0'); ?>; }
#nav a:hover span { border-color:<?php pickColor($fg4,'#448'); ?> }
#three { background:<?php pickColor($fg3,'#e8e8e8'); ?>;height:3px; }
#four { padding-top:20px;padding-bottom:25px;border-bottom:1px solid <?php pickColor($fg3,'#aaa'); ?>;min-height:1200px; }
#five { background:<?php pickColor($fg2,'#aaa'); ?>;padding-top:25px;padding-bottom:10px; }
#environmentalWarning { position: absolute;bottom: 10px;right: 50px;color:white;font-size:14px;}
</style>

<!--[if IE]><style>.clearfix { zoom: 1;display: block; }</style><![endif]-->

<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<!--
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
-->

</head>
<body>

<div id="one">
<div id="header">
When news hits you hard, maul things. <a href="/" rel="home">Bear News: Today's top stories for bearkind</a>
</div>
</div>
<div id="floatingTop">
<div id="two">
<div id="nav">
<a href="javascript:void(0)"><span>Local</span></a><a href="javascript:void(0)"><span>National</span></a><a href="javascript:void(0)"><span>World</span></a><a href="javascript:void(0)"><span>Sports</span></a><a href="javascript:void(0)"><span>Business</span></a><a href="javascript:void(0)"><span>Classifieds</span></a><a href="/" id="floatHome" rel="home">Bear News: Today's top stories for bearkind</a>
</div>
</div>
<div id="three"></div>
</div>

<div id="four">
<div class="container">
</div>
</div>
<div id="five">
<div>
<img src="images/BNB_small.png" />
</div>
<div id="environmentalWarning">
<img style="position: absolute;top: -5px;left: -35px;" src="images/tree.png" />Please drop dead if you are going to print this website
</div>
</div>
<script>
$(window).scroll(function() {
		if ($(this).scrollTop() > 134) {
			$('#floatingTop').addClass('fixIt');
		} else if($('#floatingTop').is('.fixIt')) {
			$('#floatingTop').removeClass('fixIt');
		}
	});
</script>
</body>
</html>